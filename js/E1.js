console.log("hello");

function calcular() {
    let edad = parseInt(document.getElementById("edad").value);
    console.log(edad);

    if(edad <= 1){
        document.getElementById("prom").textContent = "140-160 pulsaciones por minuto";
        let pulsaciones1 = edad * 140 * 525.960;
        let pulsaciones2 = edad * 160 * 525.960;
        document.getElementById("pulsasiones").textContent =""+pulsaciones1.toFixed(2)+" - "+pulsaciones2.toFixed(2)+" pulsaciones a lo largo de su vida";
    } else if(edad <= 12){
        document.getElementById("prom").textContent = "115-110 pulsaciones por minuto";
        let pulsaciones1 = edad * 115 * 525.960;
        let pulsaciones2 = edad * 110 * 525.960;
        document.getElementById("pulsasiones").textContent =""+pulsaciones1.toFixed(2)+" - "+pulsaciones2.toFixed(2)+" pulsaciones a lo largo de su vida";
    } else if(edad <= 40){
        document.getElementById("prom").textContent = "80-70 pulsaciones por minuto";
        let pulsaciones1 = edad * 80 * 525.960;
        let pulsaciones2 = edad * 70 * 525.960;
        document.getElementById("pulsasiones").textContent =""+pulsaciones1.toFixed(2)+" - "+pulsaciones2.toFixed(2)+" pulsaciones a lo largo de su vida";
    } else if(edad > 40){
        document.getElementById("prom").textContent = "70-60 pulsaciones por minuto";
        let pulsaciones1 = edad * 70 * 525.960;
        let pulsaciones2 = edad * 60 * 525.960;
        document.getElementById("pulsasiones").textContent =""+pulsaciones1.toFixed(2)+" - "+pulsaciones2.toFixed(2)+" pulsaciones a lo largo de su vida";
    }
}
